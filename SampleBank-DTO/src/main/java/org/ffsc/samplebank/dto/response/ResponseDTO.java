package org.ffsc.samplebank.dto.response;

public class ResponseDTO<T> {

	private String code;
	private String message;
	private T data;

	public ResponseDTO() {
	}

	public ResponseDTO(ResponseMessage responseMessage) {
		this.code = responseMessage.getCode();
		this.message = responseMessage.getMessage();
	}

	public String getCode() {
		return code;
	}
	
	public void setCode(String responseCode) {
		this.code =  responseCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String responseMessage) {
		this.message = responseMessage;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}
}