package org.ffsc.samplebank.dto.account;

import java.math.BigDecimal;

public class Account {
	
	private String ownerCpf;
	private BigDecimal balance;
	
	public Account() {
	}
		
	public BigDecimal getBalance() {
		return balance;
	}
	
	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	public String getOwnerCpf() {
		return ownerCpf;
	}
	
	public void setOwnerCpf(String ownerCpf) {
		this.ownerCpf = ownerCpf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((ownerCpf == null) ? 0 : ownerCpf.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (ownerCpf == null) {
			if (other.ownerCpf != null)
				return false;
		} else if (!ownerCpf.equals(other.ownerCpf))
			return false;
		return true;
	}
}