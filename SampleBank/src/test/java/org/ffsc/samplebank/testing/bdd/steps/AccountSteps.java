package org.ffsc.samplebank.testing.bdd.steps;

import java.math.BigDecimal;

import org.ffsc.samplebank.core.service.AccountService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;

public class AccountSteps {
	
	private AccountService service = new AccountService();
	
	private String documentId;
	private boolean ret;
	
	@Given("the owner document id is $documentId")
	public void givenOwnerDocumentId(@Named("documentId")String documentId){
		this.documentId = documentId;
	}
	
	@Given("I have an account with docId: $documentId and balance: $initialBalance")
	public void givenExistingAccount(@Named("documentId")String documentId, 
											@Named("initialBalance")String initialBalance){
		this.documentId = documentId;
		this.ret = service.createAccount(documentId);
		service.makeDeposit(documentId, initialBalance);
	}
	
	@When("the system tries to create a new account")
	public void createNewAccount(){
		ret = service.createAccount(documentId);
	}
	
	@When("the account receives a deposit of amount: $amount")
	public void makeDeposit(@Named("amount")String amount){
		service.makeDeposit(documentId, amount);
	}
	
	@Then("the account $shouldShouldnot be created")
	public void accountShouldOrNotBeCreated(@Named("shouldShouldnot")String shouldShouldnot){
		if("should".equalsIgnoreCase(shouldShouldnot)){
			Assert.assertTrue(ret);
		}else{
			Assert.assertFalse(ret);
		}
	}
	
	@Then("the account balance should be $balance")
	public void accountBalanceShouldBe(@Named("balance")String expectedBalance){
		BigDecimal accountBalance = service.getAccountBalance(documentId);
		BigDecimal expected = new BigDecimal(expectedBalance);
		Assert.assertTrue("Expected: "+expectedBalance+" was: "+accountBalance.toString(), accountBalance.compareTo(expected) == 0);
	}
}