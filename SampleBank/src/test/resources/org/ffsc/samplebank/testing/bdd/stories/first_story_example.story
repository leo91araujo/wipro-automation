First Test Example Account System

Narrative:
In order to ensure that the balance feature are working
As a system
I want to create accounts and make operations that changes account balances

Scenario: Create an new Account
Given the owner document id is 123
When the system tries to create a new account
Then the account should be created
And the account balance should be 0.00

Scenario: Verify that system cant create duplicate accounts
Given the owner document id is 123
When the system tries to create a new account
Then the account shouldnot be created

Scenario: Verify that system can make deposits
Given I have an account with docId: 1234 and balance: 200.00
When the account receives a deposit of amount: 100.00
Then the account balance should be 300.00