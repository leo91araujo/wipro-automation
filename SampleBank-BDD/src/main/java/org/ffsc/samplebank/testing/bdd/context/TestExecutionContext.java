package org.ffsc.samplebank.testing.bdd.context;

import java.util.ArrayList;
import java.util.List;

import org.ffsc.samplebank.dto.account.Account;
import org.ffsc.samplebank.dto.response.ResponseDTO;
import org.springframework.stereotype.Component;

@Component
public class TestExecutionContext {

	private ResponseDTO<?> lastResponse;
	private List<Account> createdAccounts = new ArrayList<>();
	
	public void registerCreatedAccount(Account account) {
		createdAccounts.add(account);
	}
	
	public void removeCreatedAccount(Account account) {
		createdAccounts.remove(account);
	}
	
	public List<Account> getCreatedAccounts(){
		return createdAccounts;
	}
	
	public ResponseDTO<?> getLastResponse() {
		return this.lastResponse;
	}
	
	public void setResponseDTO(ResponseDTO<?> responseDTO) {
		this.lastResponse = responseDTO;
	}
}