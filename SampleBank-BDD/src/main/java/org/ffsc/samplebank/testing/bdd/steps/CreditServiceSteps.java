package org.ffsc.samplebank.testing.bdd.steps;

import org.ffsc.samplebank.testing.bdd.service.CreditTestService;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CreditServiceSteps {

	@Autowired
	private CreditTestService creditTestService;

	@When("a loan of $loanAmmount is requested for the account $cpf")
	public void whenALoAnRequestIsMadeForTheUser(
			@Named("loanAmmount") String ammount, @Named("cpf") String cpf) {
		
		creditTestService.makeLoan(ammount, cpf);
	}
}
