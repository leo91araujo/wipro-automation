package org.ffsc.samplebank.testing.bdd.lifecycle;

import java.util.List;

import org.ffsc.samplebank.dto.account.Account;
import org.ffsc.samplebank.testing.bdd.context.TestExecutionContext;
import org.ffsc.samplebank.testing.bdd.service.AccountTestService;
import org.jbehave.core.annotations.AfterScenario;
import org.jbehave.core.annotations.AfterScenario.Outcome;
import org.jbehave.core.annotations.ScenarioType;
import org.springframework.beans.factory.annotation.Autowired;

public class ScenarioLifecycle {

	@Autowired
	private TestExecutionContext context;

	@Autowired
	private AccountTestService accountService;

	@AfterScenario(uponType = ScenarioType.ANY, uponOutcome = Outcome.ANY)
	public void tearDownScenario() {
		
		List<Account> accounts = context.getCreatedAccounts();
		
		for (Account account : accounts) {
			if (account != null) {
				accountService.deleteAccount(account.getOwnerCpf());
			}
		}
		
		context.getCreatedAccounts().clear();
	}
}