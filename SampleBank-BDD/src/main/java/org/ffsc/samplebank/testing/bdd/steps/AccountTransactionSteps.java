package org.ffsc.samplebank.testing.bdd.steps;

import java.math.BigDecimal;

import org.ffsc.samplebank.dto.account.Account;
import org.ffsc.samplebank.dto.response.ResponseDTO;
import org.ffsc.samplebank.testing.bdd.context.TestExecutionContext;
import org.ffsc.samplebank.testing.bdd.service.AccountTestService;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AccountTransactionSteps {
	
	@Autowired
	private TestExecutionContext context;
	
	@Autowired
	private AccountTestService accountService;
	
	@Given("an account for cpf number $cpf exists in the system")
	public void givenAnAccountExists(@Named("cpf") String cpf) {
		accountService.createAccount(cpf);
	}
	
	@Given("an account for cpf number $cpf that has the balance $previousBalance")
	public void givenAnAccountExistsAndHasBalnce(@Named("cpf") String cpf, 
			@Named("previousBalance") String previousBalance) {
		accountService.createAccount(cpf);
		accountService.makeDeposit(cpf, previousBalance);
	}
	
	@When("a deposit of $ammount is made for the account $cpf")
	public void whenMakeADeposit(@Named("ammount") String ammount, @Named("cpf") String cpf) {
		accountService.makeDeposit(cpf, ammount);
	}
	
	@When("the account $cpf informations are requested")
	public void whenRequestAccountInfo(@Named("cpf") String cpf) {
		accountService.getAccount(cpf);
	}	
	
	@When("a transfer of $ammount is made from the account $cpfOrigin to the account $cpfDestination")
	public void whenMakeADeposit(@Named("ammount") String ammount, 
								 @Named("cpfOrigin") String cpfOrigin,
								 @Named("cpfDestination") String cpfDestination) {							
		accountService.makeTransfer(cpfOrigin, cpfDestination, ammount);
	}
	
	@Then("the response code should be $responseCode and the message should be $message")
	public void assertResponseCodeAndMessage(@Named("responseCode") String responseCode,
			@Named("message") String message) {
		
		ResponseDTO<?> responseDTO = context.getLastResponse();
		
		Assert.assertEquals("Expected to get the error code: " + responseCode + " but got: " + responseDTO.getCode()
				, responseCode, responseDTO.getCode()); 
		
		Assert.assertEquals("Expected to get the message: " + message + " but got: " + responseDTO.getMessage()
				, message, responseDTO.getMessage()); 
	}
	
	@Then("an account should exist and it should have the balance $actualBalance")
	@SuppressWarnings("unchecked")
	public void assertAccountExistsAndHasBalance(@Named("actualBalance") String actualBalance) {
		
		ResponseDTO<Account> responseDTO = (ResponseDTO<Account>) context.getLastResponse();
		
		Account account = responseDTO.getData();
		
		BigDecimal expectedBalance = new BigDecimal(actualBalance);
		
		Assert.assertNotNull("Account does not exists. Response data is null.", account);
		Assert.assertEquals("Account balance is incorrect. Expected: " + expectedBalance + " but got: " + account.getBalance(),
				expectedBalance, account.getBalance());
	}
}