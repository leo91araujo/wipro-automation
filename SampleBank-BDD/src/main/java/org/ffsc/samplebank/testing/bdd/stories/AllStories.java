package org.ffsc.samplebank.testing.bdd.stories;

import java.util.List;

import org.ffsc.samplebank.testing.bdd.AbstractStories;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.StoryFinder;

public class AllStories extends AbstractStories {

	@Override
	protected List<String> storyPaths() {
		return new StoryFinder().findPaths(CodeLocations.
				codeLocationFromPath("src/main/resources"), "**/*_story.story", "");
	}
}