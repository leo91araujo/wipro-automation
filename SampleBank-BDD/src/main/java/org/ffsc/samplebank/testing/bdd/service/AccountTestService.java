package org.ffsc.samplebank.testing.bdd.service;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.ffsc.samplebank.dto.account.Account;
import org.ffsc.samplebank.dto.response.ResponseDTO;
import org.ffsc.samplebank.testing.bdd.context.TestExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class AccountTestService {

	@Value("${sbservices.endpoint}")
	private String SB_API_ENDPOINT;

	private static final String ACCOUNT_CREATE_URL = "accounts/?ownerCpf={ownerCpf}";
	private static final String ACCOUNT_DELETE_URL = "accounts/?ownerCpf={ownerCpf}";
	private static final String ACCOUNT_DEPOSIT_URL = "accounts/{ownerCpf}/deposit?value={value}";
	private static final String ACCOUNT_GET_URL = "accounts/{ownerCpf}";
	private static final String ACCOUNT_WITHDRAW_URL = "accounts/{ownerCpf}/withdraw?value={value}";
	private static final String ACCOUNT_TRANSFER_URL = "accounts/{ownerCpf}/transfer?targetCpf={targetCpf}&value={value}";

	@Autowired
	private TestExecutionContext context;

	@Autowired
	private RestTemplate restTemplate;

	public ResponseDTO<Account> createAccount(String ownerCpf) {

		String uri = SB_API_ENDPOINT.concat(ACCOUNT_CREATE_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = Collections.singletonMap("ownerCpf",
				ownerCpf);

		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.POST, httpEntity, typeRef, params);

		responseDTO = response.getBody();

		if (responseDTO.getData() != null) {
			Account account = responseDTO.getData();

			context.registerCreatedAccount(account);
		}
		
		context.setResponseDTO(responseDTO);
		
		return responseDTO;
	}
	
	
	public ResponseDTO<Account> getAccount(String ownerCpf) {

		String uri = SB_API_ENDPOINT.concat(ACCOUNT_GET_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = Collections.singletonMap("ownerCpf",
				ownerCpf);

		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.GET, httpEntity, typeRef, params);

		responseDTO = response.getBody();
		
		context.setResponseDTO(responseDTO);

		return responseDTO;
	}
	
	
	public ResponseDTO<Account> makeDeposit(String ownerCpf, String value) {
		
		String uri = SB_API_ENDPOINT.concat(ACCOUNT_DEPOSIT_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = new HashMap<>();

		params.put("ownerCpf", ownerCpf);
		params.put("value", value);
		
		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.PUT, httpEntity, typeRef, params);

		responseDTO = response.getBody();
		
		context.setResponseDTO(responseDTO);

		return responseDTO;
	}
	
public ResponseDTO<Account> makeTransfer(String ownerCpf, String destCpf, String value) {
		
		String uri = SB_API_ENDPOINT.concat(ACCOUNT_TRANSFER_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = new HashMap<>();

		params.put("ownerCpf", ownerCpf);
		params.put("targetCpf", destCpf);
		params.put("value", value);
		
		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.PUT, httpEntity, typeRef, params);

		responseDTO = response.getBody();
		
		context.setResponseDTO(responseDTO);

		return responseDTO;
	}
	
	public ResponseDTO<Account> makeWithdraw(String ownerCpf, String value) {

		String uri = SB_API_ENDPOINT.concat(ACCOUNT_WITHDRAW_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = new HashMap<>();

		params.put("ownerCpf", ownerCpf);
		params.put("value", value);
		
		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.PUT, httpEntity, typeRef, params);

		responseDTO = response.getBody();
		
		context.setResponseDTO(responseDTO);

		return responseDTO;
	}

	
	public ResponseDTO<Account> deleteAccount(String ownerCpf) {

		String uri = SB_API_ENDPOINT.concat(ACCOUNT_DELETE_URL);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = Collections.singletonMap("ownerCpf",
				ownerCpf);

		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.DELETE, httpEntity, typeRef, params);

		responseDTO = response.getBody();

		context.setResponseDTO(responseDTO);
		
		return responseDTO;
	}
}