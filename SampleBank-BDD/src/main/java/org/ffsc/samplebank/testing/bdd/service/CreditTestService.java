package org.ffsc.samplebank.testing.bdd.service;

import java.util.HashMap;
import java.util.Map;

import org.ffsc.samplebank.dto.account.Account;
import org.ffsc.samplebank.dto.response.ResponseDTO;
import org.ffsc.samplebank.testing.bdd.context.TestExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class CreditTestService {

	@Value("${sbservices.endpoint}")
	private String SB_API_ENDPOINT;

	private static final String CREDIT_MAKE_LOAN_URI = "accounts/credit/{ownerCpf}/loan?value={value}";

	@Autowired
	private TestExecutionContext context;

	@Autowired
	private RestTemplate restTemplate;
	
	
	public ResponseDTO<Account> makeLoan(String ammount, String ownerCpf) {
		
		String uri = SB_API_ENDPOINT.concat(CREDIT_MAKE_LOAN_URI);

		ResponseDTO<Account> responseDTO = null;

		HttpHeaders headers = new HttpHeaders();

		headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

		HttpEntity<String> httpEntity = new HttpEntity<>(headers);

		Map<String, String> params = new HashMap<>();

		params.put("ownerCpf", ownerCpf);
		params.put("value", ammount);
		
		ParameterizedTypeReference<ResponseDTO<Account>> typeRef = new ParameterizedTypeReference<ResponseDTO<Account>>() {
		};

		ResponseEntity<ResponseDTO<Account>> response = restTemplate.exchange(
				uri, HttpMethod.PUT, httpEntity, typeRef, params);

		responseDTO = response.getBody();
		
		context.setResponseDTO(responseDTO);

		return responseDTO;
	}
}