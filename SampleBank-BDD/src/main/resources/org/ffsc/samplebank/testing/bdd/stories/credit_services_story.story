Credit Services Story

Narrative:
In order to ensure that the credit service API is working
As a consumer/client
I want to perform and check credit operations

Scenario: The bank user that has the credit approved should be able to make a loan
Given an account for cpf number <cpf> that has the balance <previousBalance>
When a loan of <loanAmmount> is requested for the account <cpf>
Then the response code should be <responseCode>  and the message should be <message>
When the account <cpf> informations are requested
Then the response code should be <responseCode>  and the message should be <message>
And an account should exist and it should have the balance <actualBalance>
Examples:
| cpf 		  |	  previousBalance	| loanAmmount	|	actualBalance	| responseCode | message 							|
| 06444870976 |	  2000.00			|	 550.00		|		2550.00		|	INF001	   | Operation completed with success 	|


Scenario: The bank user that does not have the credit approved should not be able to make a loan
Given an account for cpf number <cpf> that has the balance <previousBalance>
When a loan of <loanAmmount> is requested for the account <cpf>
Then the response code should be <responseCode>  and the message should be <message>
When the account <cpf> informations are requested
Then an account should exist and it should have the balance <actualBalance>
Examples:
| cpf 		  |	  previousBalance	| loanAmmount	|	actualBalance	| responseCode | message 																				|
| 06444870976 |	  	150.00			|	 1500.00	|	150.00			|	ERR006	   | Insufficient balance in account for the loan operation. Minimum of $2000 is required 	|
| 06444870976 |	  	3000.00			|	 2500.00	|	3000.00			|	ERR005	   | The requested loan ammount exceeds the available credit limit						 	|


Scenario: The bank user can have just one pending loan at a time
Given an account for cpf number <cpf> that has the balance <previousBalance>
When a loan of <loanAmmount> is requested for the account <cpf>
Then the response code should be INF001 and the message should be Operation completed with success
When the account <cpf> informations are requested
Then an account should exist and it should have the balance 3600.00
When a loan of <loanAmmount> is requested for the account <cpf>
Then the response code should be ERR007 and the message should be There is a pending loan for this account
When the account <cpf> informations are requested
Then an account should exist and it should have the balance 3600.00
Examples:
| cpf 		  |	  previousBalance	| loanAmmount	|
| 06444870976 |	  	3000.00			|	 600.00		|
