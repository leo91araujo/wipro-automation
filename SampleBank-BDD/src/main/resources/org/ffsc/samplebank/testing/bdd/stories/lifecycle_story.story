A story is a collection of scenarios
 
Narrative:
In order to communicate effectively to the business some functionality
As a development team
I want to use Behaviour-Driven Development
 
Lifecycle:
Before:
!-- Given a step that is executed before each scenario
!--After:
Outcome: ANY    
!--Given a step that is executed after each scenario regardless of outcome
Outcome: SUCCESS 
!--Given a step that is executed after each successful scenario
Outcome: FAILURE 
!-- Given a step that is executed after each failed scenario