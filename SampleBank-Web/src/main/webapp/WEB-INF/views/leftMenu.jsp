<script type="text/javascript" src="resources/scripts/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="resources/scripts/menu.js" /></script>
<div class="sb-admin-menu">
	<dl>
		<dt>
			<a href="#">Accounts</a>
		</dt>
		<dd>
			<a href="addAccount" class="linkButton">Create Account</a>
			<a href="deposit" class="linkButton">Deposit</a>
			<a href="withdrawn" class="linkButton">Withdrawn</a>
			<a href="transfer" class="linkButton">Transfer</a>
		</dd>
	</dl>
	<dl>
		<dt>
			<a href="#">Loans</a>
		</dt>
		<dd>
			<a href="makeLoan" class="linkButton">Make Loan</a>
			<a href="consultLoan" class="linkButton">Consult Loan</a>
		</dd>
	</dl>
</div>