package org.ffsc.samplebank.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.ffsc.samplebank.core.domain.Account;
import org.ffsc.samplebank.web.controller.api.AccountController;
import org.ffsc.samplebank.web.response.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class AdminController {
	
	@Autowired
	private AccountController accountController;
	
	@RequestMapping(value = {"/addAccount"}, method = RequestMethod.GET)
	public String rootAddAccount(Model model) {
		model.addAttribute("account",  new Account(""));
		return "addAccount";
	}
	
	@RequestMapping(value = {"/addAccount"}, method = RequestMethod.POST)
	public String saveAccount(Account account, HttpServletResponse httpResponse, Model model) {
		String numericCpf = account.getOwnerCpf().replaceAll("[\\.-]", "");
		ResponseDTO<Account> responseDTO = accountController.createAccount(numericCpf, httpResponse);
		model.addAttribute("responseDTO", responseDTO);
				
		return rootAddAccount(model);
	}
}