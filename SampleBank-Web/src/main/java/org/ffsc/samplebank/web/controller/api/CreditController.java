package org.ffsc.samplebank.web.controller.api;

import java.math.BigDecimal;

import javax.servlet.http.HttpServletResponse;

import org.ffsc.samplebank.core.domain.Account;
import org.ffsc.samplebank.core.service.AccountService;
import org.ffsc.samplebank.core.service.CreditService;
import org.ffsc.samplebank.web.response.ResponseDTO;
import org.ffsc.samplebank.web.response.ResponseMessage;
import org.ffsc.samplebank.web.validator.BusinessValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
@RequestMapping("api/accounts/credit")
public class CreditController {

	@Autowired
	private AccountService accountService;
	
	@Autowired
	private CreditService creditService;
	
	
	@RequestMapping(value = "/{ownerCpf}/loan", method = RequestMethod.PUT, produces = { "application/json" })
	@ResponseBody
	public ResponseDTO<Account> makeLoan(@PathVariable String ownerCpf,
			@RequestParam String value, HttpServletResponse response) {

		ResponseDTO<Account> responseDTO = new ResponseDTO<>();

		if (BusinessValidator.isCpfValid(ownerCpf)) {
			
			Account account = accountService.getAccount(ownerCpf);
			
			if (account != null) {
				if (BusinessValidator.isAmmountfValid(value)) {
					
					/*
					 * Credit operation ...
					 */
					if(!account.hasPendingLoan()) {
						if(account.getBalance().compareTo(creditService.getMinimumBalance()) >= 0) {
							
							BigDecimal loanAmmount = new BigDecimal(value);
							
							if(loanAmmount.compareTo(creditService.getAllowedAmount(account)) <= 0) {
								
								creditService.makeLoan(ownerCpf, loanAmmount);
								responseDTO.setMessage(ResponseMessage.SUCCESS);
								
							} else {
								responseDTO.setMessage(ResponseMessage.LOAN_CREDIT_EXCEEDED);
							}
							
						} else {
							responseDTO.setMessage(ResponseMessage.LOAN_NOT_ENOUGH_BALANCE);
						}
						
					} else {
						responseDTO.setMessage(ResponseMessage.LOAN_PENDING_FOR_ACCOUNT);
					}
					
				} else {
					responseDTO.setMessage(ResponseMessage.INVALID_AMMOUNT);
				}

			} else {
				responseDTO.setMessage(ResponseMessage.ACCOUNT_NOT_FOUND);
			}

		} else {
			responseDTO.setMessage(ResponseMessage.INVALID_CPF);
		}
		
		response.setStatus(HttpStatus.OK.value());

		return responseDTO;
	}
}